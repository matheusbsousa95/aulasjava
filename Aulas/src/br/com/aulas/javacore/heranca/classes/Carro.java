package br.com.aulas.javacore.heranca.classes;

public class Carro {
	public static final double VELOCIADE_LIMITE = 300;
	private String nome;
	private String marca;
	
	
	@Override
	public String toString() {
		return "Carro [nome=" + nome + ", marca=" + marca + "]";
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	
}
