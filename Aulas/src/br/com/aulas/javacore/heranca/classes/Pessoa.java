package br.com.aulas.javacore.heranca.classes;

public class Pessoa {

	protected String nome;
	protected String cpf;
	protected Endereco endereco;
	
	public void print() {
		System.out.println("-----Dados Pessoa-----");
		System.out.println("Nome: " + this.nome);
		System.out.println("CPF: " + this.cpf);
		System.out.println("Endereco: " + this.endereco.getBairro() + " - " + this.endereco.getRua());
	}

	public Pessoa(String nome) {
		super();
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	
	
	
	
}
