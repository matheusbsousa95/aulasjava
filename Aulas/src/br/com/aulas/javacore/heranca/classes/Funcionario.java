package br.com.aulas.javacore.heranca.classes;

public class Funcionario extends Pessoa {
	private double salario;
	
	

	public Funcionario(String nome) {
		super(nome);
	}

	public void print() {
		super.print();
		System.out.println("Sal�rio: " + this.salario);
		imprimeReciboDePagamento();
	}
	
	public void imprimeReciboDePagamento() {
		System.out.println("Eu " + super.nome + " recebi um sal�rio no valor de: R$ " + this.salario);
	}
	
	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
		
	
}
