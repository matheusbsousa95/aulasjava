package br.com.aulas.javacore.exception.testes;

public class MultiExceptions {

	public static void main(String[] args) {

		try {
			throw new ArrayIndexOutOfBoundsException();
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Dentro do ArrayIndexOutOfBoundsException");
		}catch (IllegalArgumentException e) {
			System.out.println("Dentro do IllegalArgumentException");
		}catch (ArithmeticException e) {
			System.out.println("Dentro do ArithmeticException");

		}
		
		
		
		
	}

}
