package br.com.aulas.javacore.exception.testes;

import br.com.aulas.javacore.exception.classes.Funcionario;
import br.com.aulas.javacore.exception.classes.Pessoa;

public class SobrescritaComExceptionTest {

	public static void main(String[] args) {
		Funcionario f = new Funcionario();
		Pessoa p = new Pessoa();
		p.salvar();
	}

}
