package br.com.aulas.javacore.exception.testes;

import br.com.aulas.javacore.exception.classes.Leitor;

public class TryWithResourcesTest {

	public static void main(String[] args) {

		lerArquivo();

	}

	public static void lerArquivo() {

		try(Leitor leitor = new Leitor()){
			
		} catch( Exception e) {
			e.printStackTrace();
		}
		
	}

}
