package br.com.aulas.javacore.exception.testes;

import java.io.File;
import java.io.IOException;

public class ExceptionTeste {
	public static void main(String[] args) {

		
		//try {
		//	divisao(10, 0);
		//} catch (RuntimeException e) {
		//	System.out.println(e.getMessage());
		//}
		//
		
		//try {
		//	criarArquivo();
		//} catch(IOException e) {
		//	e.printStackTrace();
		//}
		abrirArquivo();
		
		
	}

	private static void divisao(int num1, int num2) {
		if (num2 == 0) {
			throw new IllegalArgumentException("Passe um valor diferente para o num2");
		}
		int result = (num1 / num2);
		System.out.println(result);

	}

	public static void criarArquivo() throws IOException{

		File file = new File("Teste.txt");
		try {
			System.out.println("Arquivo criado ? " + file.createNewFile());
			System.out.println("criando arquivo");			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static String abrirArquivo() {

		File file = new File("Teste.txt");
		try {
			System.out.println("Abrindo arquivo");		
			System.out.println("Executndo leitura do arquivo");
			//throw new Exception();
			System.out.println("Escrevendo no arquivo");
			return "Valor";
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Fechando o arquivo");			
			
		}
		return null;
	}
}
