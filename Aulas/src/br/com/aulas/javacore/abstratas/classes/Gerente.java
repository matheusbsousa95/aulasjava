package br.com.aulas.javacore.abstratas.classes;

public class Gerente extends Funcionario {
	private double pnl;

	public Gerente (String nome, String clt, double salario, double pnl ) {
		super(nome, clt,salario);
		this.pnl = pnl;
	}
		
	@Override
	public void calculaSalario() {
		this.salario = this.salario + this.pnl;
	}

	@Override
	public String toString() {
		return "Gerente [ nome " + nome + " CLT " + " sal�rio " + salario + " pnl " + pnl + "]";
	}

	public double getPnl() {
		return pnl;
	}

	public void setPnl(double pnl) {
		this.pnl = pnl;
	}
	
	
	
	
}
