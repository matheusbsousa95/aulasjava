package br.com.aulas.javacore.basico.teste;

import br.com.aulas.javacore.basico.classes.Cliente;

public class ClienteTest {

	public static void main(String[] args) {

		Cliente c1 = new Cliente("Matheus", "052172191-14", "19/09/1995", 3154596);

		c1.imprime();
		c1.setTelefones(new String[] {"993193090", "987456813"});
		c1.imprime();

	}

}
