package br.com.aulas.javacore.basico.classes;

public class Cliente {

	private String nome;
	private String cpf;
	private String dataNasc;
	private int rg;
	private String[] telefones;

	public Cliente(String nome, String cpf, String dataNasc, int rg, String[] telefones) {
		this(nome, cpf, dataNasc, rg);
		this.setTelefones(telefones);

	}

	public Cliente(String nome, String cpf, String dataNasc, int rg) {
		this.setNome(nome);
		this.setCpf(cpf);
		this.setDataNasc(dataNasc);
		this.setRg(rg);

	}

	public void imprime() {
		System.out.println(this.nome);
		System.out.println(this.cpf);
		System.out.println(this.dataNasc);
		System.out.println(this.rg);
		if (telefones != null) {
			for (String telefone : telefones) {
				System.out.println("Telefone: " + telefone);
			}
		}
	}

	public String[] getTelefones() {
		return telefones;
	}

	public void setTelefones(String[] telefones) {
		this.telefones = telefones;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(String dataNasc) {
		this.dataNasc = dataNasc;
	}

	public int getRg() {
		return rg;
	}

	public void setRg(int rg) {
		this.rg = rg;
	}

}
