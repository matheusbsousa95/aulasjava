package br.com.aulas.javacore.basico.classes;

public class Carro {
	private String nome;
	private String placa;
	private String modelo;
	private double velocidadeMaxima;
	public static double velocidadeLimite = 240 ;
	private String[][] interior;
	
	public Carro() {
		
	}
	
	public Carro(String nome, double velocidadeMaxima) {
		this.nome = nome;
		this.velocidadeMaxima = velocidadeMaxima;
	}

	public void imprime() {
		System.out.println("<--------------------------->");
		System.out.println("Nome: "+this.nome);
		System.out.println("Velocidade M�xima: "+this.velocidadeMaxima);
		System.out.println("Velocidade limite: "+ velocidadeLimite);
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public double getVelocidadeMaxima() {
		return velocidadeMaxima;
	}

	public void setVelocidadeMaxima(double velocidadeMaxima) {
		this.velocidadeMaxima = velocidadeMaxima;
	}

	public static double getVelocidadeLimite() {
		return velocidadeLimite;
	}
	
	public static void setVelocidadeLimite(double velocidadeLimite) {
		Carro.velocidadeLimite = velocidadeLimite;
	}
	
	public String[][] getInterior() {
		return interior;
	}

	public void setInterior(String[][] interior) {
		this.interior = interior;
	}
	


}
