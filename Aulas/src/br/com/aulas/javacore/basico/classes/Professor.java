package br.com.aulas.javacore.basico.classes;

public class Professor {

	public String nome;
	public String matricula;
	public String cpf;

	public void mostrarValores(Professor prof) {
		System.out.println("O nome �: " + prof.nome);
		System.out.println("A matr�cula �: " + prof.matricula);
		System.out.println("O CPF �: " + prof.cpf);
	}

}
