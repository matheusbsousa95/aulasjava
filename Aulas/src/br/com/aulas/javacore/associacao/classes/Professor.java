package br.com.aulas.javacore.associacao.classes;

public class Professor {
	private Seminario[] seminario;
	private String nome;
	private String especialidade;
	
	public Professor(String nome, String especialidade) {
		this.nome = nome;
		this.especialidade = especialidade;
	}
	
	public void print() {
		System.out.println("-----------Relatório de seminários------------");
		System.out.println("Nome do professor: " + this.nome);
		System.out.println("Especialidade: " + this.especialidade);
		System.out.println("Seminários participantes: ");
		for (Seminario sem: seminario) {
			System.out.println(sem.getTitulo() + "\n ");
		}
	}
	
	public Seminario[] getSeminario() {
		return seminario;
	}
	public void setSeminario(Seminario[] seminario) {
		this.seminario = seminario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEspecialidade() {
		return especialidade;
	}
	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}
	
	
	
}
