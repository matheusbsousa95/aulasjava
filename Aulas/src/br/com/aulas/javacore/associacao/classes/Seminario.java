package br.com.aulas.javacore.associacao.classes;

public class Seminario {
	private Professor professor;
	private Local local;
	private Aluno[] aluno;
	private String titulo;
	
	public Seminario() {
		
	}
	public Seminario(String titulo) {
		this.titulo = titulo;
	}
	
	public void print() {
		System.out.println("Titulo: "+ this.titulo);
		System.out.println("Palestrante: Professor " + this.professor);
		System.out.println("Local: "+ this.local);
		System.out.println("Participantes: ");
		for(Aluno alu : aluno) {
			System.out.println(alu.getNome());
		}
	}
	public String getTitulo() {
		return this.titulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public Aluno[] getAluno() {
		return aluno;
	}

	public void setAluno(Aluno[] aluno) {
		this.aluno = aluno;
	}
}
