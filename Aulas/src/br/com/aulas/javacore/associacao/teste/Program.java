package br.com.aulas.javacore.associacao.teste;

import br.com.aulas.javacore.associacao.classes.Aluno;
import br.com.aulas.javacore.associacao.classes.Local;
import br.com.aulas.javacore.associacao.classes.Professor;
import br.com.aulas.javacore.associacao.classes.Seminario;

public class Program {

	public static void main(String[] args) {
		Aluno aluno = new Aluno("Matheus", 22);
		Aluno aluno2 = new Aluno("Micky", 5);
		Seminario seminario = new Seminario("Como ficar rico rapido");
		Local local = new Local("Sobradinho", "DF");
		Professor professor = new Professor("Yod�o", "Arte do Trab");
		
		seminario.setAluno(new Aluno[] {aluno, aluno2});
		seminario.setProfessor(professor);
		professor.setSeminario(new Seminario[] {seminario});
		seminario.setLocal(local);
		
		aluno.setSeminario(seminario);
		//aluno.print();
		
		//professor.print();
		
		seminario.print();
		
	}

}
